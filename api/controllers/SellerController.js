/**
 * SellerController
 *
 * @description :: Server-side logic for managing sellers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    printall: function (req, res) {

        Item.find().then(function (items) {
            res.view('qr', {
                items: items
            });
        });
    },

    login: function (req, res) {
        var args = req.body;

        if (! args.password || ! args.email){
            return res.badRequest('Missing credentials');
        }

        Seller.findOne()
            .where({email: args.email})
            .then(function (seller) {
                if (!seller) {
                    return res.badRequest('Wrong email address');
                }

                Seller.checkPass(seller.password, args.password, function (err, result) {
                    if (err) {
                        return res.serverError(err);
                    }

                    if (result) {
                        req.session.user = seller;
                        res.ok(seller);
                    } else {
                        res.badRequest('Wrong password');
                    }
                })
            })
            .catch(function (err) {
                res.serverError(err);
            });
    },

    logout: function (req, res) {
      delete req.session.user;

      res.ok();
    }

};

