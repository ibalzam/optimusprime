/**
 * PurchaseController
 *
 * @description :: Server-side logic for managing purchases
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    'checkout': function(req, res) {
        if (!req.body.item_ids) {
            return res.send("No ids supplied", 403);
        }

        var price = 0;
        Item.find()
            .where({id: req.body.item_ids})
            .exec(function (err, response) {
                _.each(response, function(item) {
                    if (item.price) {
                        price = price + parseInt(item.price);
                    }
                });

                Purchase.create({'purchase_price': price, 'purchase_status': 'open'}, function(err, purchase) {
                    console.log(purchase);
                    return res.send({url: "/sale/payment/" + purchase.id});
                });
            });
    }
};

