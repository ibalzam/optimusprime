/**
 * QrcodeController
 *
 * @description :: Server-side logic for managing qrcodes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var qr = require('qr-image'),
    fs = require('fs');
    // _ = require('lodash');

module.exports = {
	generate: function(req, res) {
        if (!req.params.id) {
            return res.send({error: 'QR Not Found'}, 500);
        }
        Item.findOne().where({id: req.params.id}).exec(function(err,item) {
            if (err) {
                return res.return(err, 500);
            }
            if (!item) {
                return res.send({error: 'no item'}, 200);
            }

            var result = _.pick(item, 'item_price', 'item_name', 'id');
            var svg_string = qr.imageSync(JSON.stringify(result), { type: 'svg' });
            res.header('Content-Type', 'image/svg+xml');
            res.send(svg_string);
        });

    }  
   
};

