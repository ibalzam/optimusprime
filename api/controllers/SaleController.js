/**
 * SaleController
 *
 * @description :: Server-side logic for managing sales
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var util = require('util'),
    braintree = require('braintree');

var gateway = braintree.connect({
    environment: braintree.Environment.Sandbox,
    merchantId: 'z4zypbwy8fbr9zh5',
    publicKey: 'kpzf8w68rrg6nsx3',
    privateKey: '2fe33d964e66b60388c65e7306f103f1'
});

module.exports = {
    'payment': function(req, res) {
        if (!req.params.id) {
            return res.view('sale/saleNotFound');
        }
        var purchase = Purchase.findOne().where({id: req.params.id}).exec(function(err, purchase) {
            if (!purchase) {
                return res.view('sale/saleNotFound');
            }
            return res.view('sale/paymentForm', {price: purchase.purchase_price});
        });
    },

    'process': function(req, res) {
        console.log("Doing process with nonce: " + req.body.nonce + ", and price: " + req.body.price);
        console.log("Doing process with nonce: ", req.body.nonce);
        gateway.transaction.sale({
            amount: req.body.price,
            paymentMethodNonce: req.body.nonce
        }, function (err, result) {
            if (result) {
                console.log("Sale > process ", result);
                return res.send({result: 'success'});
            }
        });
    },
    'token': function(req, res) {
        gateway.clientToken.generate({}, function (err, response) {
            return res.send({token: response.clientToken}, 200);
        });
    },
    'thank-you': function(req, res) {
        return res.view('sale/thankYou', 200);
    }
};

