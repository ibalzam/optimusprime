/**
* Seller.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {
  attributes: {
  	sale : {
  		collection : "Sale",
  		via: "seller" 
  	},
    email: {
      type: 'string',
      unique: true,
      required: true
    },
    password: {
      type: 'string',
      required: true
    },

    toJSON: function () {
      var obj = this.toObject();
      delete obj.password;

      return obj;
    }
  },

  beforeCreate: function (user, cb) {
    bcrypt.genSalt(10, function (err, salt) {
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          return cb(err);
        }
        user.password = hash;

        cb();
      });
    });
  },

  checkPass: function (hash, password, cb) {
    bcrypt.compare(password, hash, cb);
  }
};

