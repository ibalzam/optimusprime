/**
* Sale.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  attributes: {
  	items : {
  		collection : "Item",
  		via: "sale" 
  	},
  	seller : {
  		model : "Seller"
  	},
    date: {
      type: 'date',
      required: true
    }
  }

};

